require 'faker'
require 'rails_helper'
require 'devise/jwt/test_helpers'

RSpec.describe 'POST /users', type: :request do
  let(:url) { '/users' }
  let(:params) do
    {
      user: {
        name: Faker::Name.name,
        email: Faker::Internet.email,
        password: Faker::Internet.password
      }
    }
  end

  describe "create user" do
    before do
      # headers = { 'Accept' => 'application/json' }
      # auth_headers = Devise::JWT::TestHelpers.auth_headers(headers, params)

      post url, params: params#, headers: auth_headers
    end

    it "have http status 200" do
      expect(response).to be_successful
    end

    it "has the billing info passed in params" do
      expect(JSON.parse(response.body)["email"]).to eq params[:user][:email]
      expect(JSON.parse(response.body)["name"]).to  eq params[:user][:name]
    end
  end
end


RSpec.describe 'POST /users/sign_in', type: :request do
  let(:user) { Fabricate(:user) }
  let(:url) { '/users/sign_in' }
  let(:params) do
    {
      user: {
        email: user.email,
        password: user.password
      }
    }
  end
  
  context 'when login params are not provided' do
    before { post url }
    
    it 'returns unathorized status' do
      expect(response.status).to eq 401
    end
  end
  
  context 'when params are correct' do
    before do
      post url, params: params
    end

    it 'returns 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns JTW token in authorization header' do
      expect(response.headers['Authorization']).to be_present
    end

    it 'returns valid JWT token' do
      token_from_request = response.headers['Authorization'].split(' ').last
      decoded_token = JWT.decode(token_from_request, ENV['DEVISE_JWT_SECRET_KEY'], true)
      expect(decoded_token.first['sub']).to be_present
    end
  end
end

RSpec.describe 'PATCH /users', type: :request do
  let(:url) { '/users' }
  let(:user) { Fabricate(:user, email: Faker::Internet.email) }
  let(:admin) { Fabricate(:user, email: Faker::Internet.email, role: 1) }

  let(:params) do
    {
      user: {
        id: user.id,
        active: true
      }
    }
  end

  describe "when user is not logged in" do
    before do
      patch url
    end

    it "returns 401" do
      expect(response).to have_http_status(401)
    end
  end

  describe "acvate user" do
    before do
      headers = { 'Accept' => 'application/json' }
      auth_headers = Devise::JWT::TestHelpers.auth_headers(headers, admin)

      patch url, params: params, headers: auth_headers
    end

    it "have http status 200" do
      expect(response).to be_successful
    end

    it "is now active" do
      expect(JSON.parse(response.body)["active"]).to eq true
    end
  end
end

RSpec.describe 'DELETE /users', type: :request do
  let(:url) { '/users' }
  let(:user) { Fabricate(:user, email: Faker::Internet.email) }
  let(:admin) { Fabricate(:user, email: Faker::Internet.email, role: 1) }
  let(:params) do
    {
      user: {
        id: user.id
      }
    }
  end

  describe "when user is not logged in" do
    before do
      patch url
    end

    it "returns 401" do
      expect(response).to have_http_status(401)
    end
  end
  
  describe "when admin is not an admin" do
    before do
      headers = { 'Accept' => 'application/json' }
      non_admin_user = Fabricate(:user, email: Faker::Internet.email)
      auth_headers = Devise::JWT::TestHelpers.auth_headers(headers, non_admin_user)

      delete url, params: params, headers: auth_headers
    end

    it "returns 401" do
      expect(response).to have_http_status(401)
    end
  end

  describe "destroy user" do
    before do
      headers = { 'Accept' => 'application/json' }
      auth_headers = Devise::JWT::TestHelpers.auth_headers(headers, admin)

      delete url, params: params, headers: auth_headers
    end

    it "have http status 204" do
      expect(response).to be_successful
    end
  end
end