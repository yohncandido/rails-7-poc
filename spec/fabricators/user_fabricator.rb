Fabricator(:user) do
  email     { |attrs| attrs[:email] ||= Faker::Internet.email }
  name      Faker::Name.name
  password  Faker::Internet.password
  role      { |attrs| attrs[:role] || 2 }
end
