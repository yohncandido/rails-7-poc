# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    return unless user.admin?
    can :update, User
    can :destroy, User
  end
end
