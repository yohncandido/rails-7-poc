class ApplicationController < ActionController::API
  rescue_from CanCan::AccessDenied do |exception|
    render status: 401
  end
end
