# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  respond_to :json

  def create
    if self.resource = warden.authenticate(auth_options)
      sign_in(resource_name, resource)
      respond_with resource
    else
      render json: { errors: { user: 'INVALID' } }, status: 401
    end
  end

  private

  def respond_with(resource, _opts = {})
    render json: resource
  end

  def respond_to_on_destroy
    head :no_content
  end
end
