# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  respond_to :json
  before_action :authenticate_user!, only: [:update, :destroy]
  # POST /resource
  def create
    build_resource(user_params)
    resource.save
    respond_with resource
  end

  # PUT /resource
  def update
    user = User.find user_update_params[:id]
    authorize! :update, user

    user.update user_update_params
    if user.errors.any?
      render json: { errors: user.errors }, status: :bad_request
    else
      render json: user
    end
  end

  # DELETE /resource
  def destroy
    user = User.find user_update_params[:id]
    authorize! :destroy, user

    if user.destroy
      render json: :no_content
    else
      render json: { errors: user.errors }, status: :bad_request
    end
  end

  protected

  def respond_with(resource, _opts = {})
    if !resource.errors.empty?
      render json: {errors: resource.errors}, status: :bad_request
    else
      render json: resource
    end
  end

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
  
  def user_update_params
    params.require(:user).permit(:id, :name, :email, :password, :password_confirmation, :role, :active)
  end
end
