# README

### Rodar migrações
```
$ bundle exec rails db:migrate
```

### Váriavies de ambiente
Gerar chave para JWT
```
$ bundle exec rake secret
```
Copiar .env.sample para .env

Colar a chave gerada para a env var `DEVISE_JWT_SECRET_KEY`


### Iniciar servidor
```
$ rails s
```

### Criar usuário admin
```
$ rails c
> User.create(email: "email@example.com", name: "Yohann", password: "123456", role: 0, active: true)
```

## Exemplo de requisições
### Login
```
POST /users/sign_in
{
    "user": {
        "email": "yohann.candido@gmail.com",
        "password": "102200"
    }
}
```
Armazenar header `authorization`

### Ativar usuário
```
PATCH/PUT /users
Headers:
Authorization: Bearer ...
{
    "user": {
        "id": 1,
        "active": "true"
    }
}
```
### Criar usuário
```
PATCH/PUT /users
{
    "user": {
        "email": "test@example.com",
        "name": "Google Robot Silva",
        "password": "123456"
    }
}
```
